<?php

namespace App\Http\Controllers;
use App\Models\Sivamed;
use Illuminate\Http\Request;
use DB;
class SivamedController extends Controller
{
    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }
    public function search_sivamed(Request $request){
        $sivamed=Sivamed::where('c_clave','CLI')->where(DB::raw("concat(TRIM(c_ruc),' ',TRIM(c_descrip))"),'like',"%".$request->buscar."%")->get();
        return compact('sivamed');
      }
    public function edit(Sivamed $sivamed)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sivamed  $sivamed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sivamed $sivamed)
    {
        //
    }


    public function tienda(Request $request){
        $tienda = Sivamed::select(DB::raw("CONCAT(c_codigo,' ',c_descrip) AS c_descrip"),'row_id')->where('c_clave','=','TDA')->get();
        return ['tienda'=>$tienda];
    }
     public function destroy(Sivamed $sivamed){
        //
    }
}
