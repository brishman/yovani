<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginFormRequest;

class LoginController extends Controller{
    public function showLoginForm(){
        return view('layouts.login');
    }
    public function login(Request $request){
        if (Auth::attempt(['user_nomb'=>$request->email,'password'=>$request->password])){
            return response()->json(['message'=>'Usuario Conectado','status'=>200]);
        }else{
            return response()->json(['message'=>'Cuenta de Usuario Incorecto','status'=>402]);
        }
  // return response()->json($request->email);
    }
    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        return redirect('/');
     }
    protected function validateLogin(Request $request){
        $this->validate($request,[
            'user_nomb' => 'required|string',
            'password' => 'required|string'
        ]);

    }
}
