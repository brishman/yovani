<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Sivamed;
use App\Http\Requests\LoginFormRequest;

class LoginController extends Controller{
    public function showLoginForm(){
        return view('layouts.login');
    }
    public function login(Request $request){
       // dd($request);
        $this->validateLogin($request);

        if (Auth::attempt(['user_nomb'=>$request->user_nomb,'password'=>$request->password])){
            session(['c_codtienda' => $request->tienda]);
            $tiendaactiva=Sivamed::where('row_id','=',$request->tienda)->get()->first();
            session(['c_tienda' => $tiendaactiva->c_descrip]);
            return response()->json(['message'=>'Usuario Conectado','status'=>200]);

//            return redirect('/sales');

        }else{
            return response()->json(['message'=>'Cuenta de Usuario Incorecto','status'=>402]);
            return redirect('/');
        }
    }

    protected function validateLogin(Request $request){
        $this->validate($request,[
            'tienda' => 'required',
            'user_nomb' => 'required|string',
            'password' => 'required|string'
        ]);

    }
}
