<?php

namespace App\Http\Controllers;

use App\Sivapedh;
use Illuminate\Http\Request;

class SivapedhController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sivapedh  $sivapedh
     * @return \Illuminate\Http\Response
     */
    public function show(Sivapedh $sivapedh)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sivapedh  $sivapedh
     * @return \Illuminate\Http\Response
     */
    public function edit(Sivapedh $sivapedh)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sivapedh  $sivapedh
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sivapedh $sivapedh)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sivapedh  $sivapedh
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sivapedh $sivapedh)
    {
        //
    }
}
