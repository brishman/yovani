<?php
namespace App\Http\Controllers;
use App\Models\Sivapedm;
use App\Models\Sivamed;
use App\Models\Sivacodm;
use App\Models\Sivapedh;
use App\Models\Sivacomh;

use App\Usuario;
use App\Sivatpc;
use App\Sivanro;
use Carbon\Carbon;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
class SivapedmController extends Controller{
    public function index()
    {
        return view('layouts.sales.index');
    }
    public function buscar_costo(Request $request){
       $sivacomh=Sivacomh::where('c_codigo','=',$request->c_codigo)->get();
       return compact('sivacomh');
    }
    public function save(Request $request){
    try {
        DB::beginTransaction();
        $tiendaactiva=Sivamed::where('row_id','=',session('c_codtienda'))->get();
        $cod=$tiendaactiva[0]->c_codigo;
         $correlativo=Sivanro::where('tienda','=', $cod)->get()->first();
         $comprobante=Sivanro::findOrFail($correlativo->id);
         $anio = Carbon::now();
         $anio = $anio->format('Y');
         $numero_pedidos=str_pad($correlativo->numero+1,7,"0",STR_PAD_LEFT)."-".$anio;
        $sivapedm=new Sivapedm();
//        $sivapedh=new Sivapedh();
        $sivapedm->c_pedido=$numero_pedidos;
        $sivapedm->f_fecha=$request->f_fecha;
        $clientes=Sivamed::where('c_ruc','=',$request->c_rucemp)->get()->first();
        $usuario=Usuario::where('id','=',Auth()->id())->get()->first();
        $tienda=Sivamed::where('c_clave','=','TDA')->where('row_id','=',session('c_codtienda'))->get()->first();
        $sivapedm->c_moneda=$tienda->c_descrip;
        $sivapedm->n_tcambio=$request->tipo_cambio;
        $sivapedm->c_clientevta=$request->c_descrip;
        $sivapedm->c_empresa=$request->c_descrip;
        $sivapedm->c_docum=$request->document;
        $sivapedm->n_margen=0;
        if($request->c_pagoen=="Soles"){
            $sivapedm->c_pagoen="NUEVOS SOLES";
        }else{
            $sivapedm->c_pagoen="DOLARES AMERICANOS";
        }
        $sivapedm->c_motivo=strtoupper($request->c_motivo);
        $sivapedm->c_tele=strtoupper($request->c_motivo);
        $sivapedm->c_telefono="";
        $sivapedm->c_codemp=$request->c_codemp;
        $sivapedm->c_codvta=$request->c_codvta;
        $sivapedm->c_direcci=$request->c_direcci;
        $sivapedm->c_rucemp=$request->c_rucemp;
        $sivapedm->c_codvta=$clientes->c_codigo;
        $sivapedm->c_codemp=$clientes->c_codigo;
        $sivapedm->c_correocli=$request->c_correocli;
        $sivapedm->c_tipocli=$clientes->c_tipocli;

        $sivapedm->n_total=$request->total;
        $sivapedm->n_credito=0;
        $sivapedm->n_lincre=0;
        $sivapedm->n_dscto=0;
  //      dd($request->total);
        $sivapedm->c_usuario=$usuario->user_nive;
        $sivapedm->c_vendedor=$usuario->user_nomb;
        $sivapedm->c_venpedido=$usuario->user_nomb;
        $sivapedm->c_tda=$tienda->c_codigo;
        $sivapedm->c_generado="N";
        $sivapedm->c_estado="V";
        $sivapedm->c_movto="S";
        $sivapedm->c_anulado="N";
        $sivapedm->c_dias="0";
        $sivapedm->c_tm="0";
        $sivapedm->c_provee="";
        $sivapedm->c_correocli="";
        $sivapedm->save();

        $detalles=$request->items;

        $sivapedm_update=Sivapedm::findOrFail($sivapedm->row_id);
        $sivapedm_update->c_numint= str_pad($sivapedm->row_id,12,"0",STR_PAD_LEFT);
        $sivapedm_update->save();
        $suma_n_bulto=0.00;
        $suma_n_tocosori=0.00;
        $suma_n_total=0.00;
        foreach ($request->items as $ep => $det) {
            $sivapedh=new Sivapedh();
            $suma_n_total=$suma_n_total+$det['total'];
            $sivapedh->c_docum=$request->document;
            $sivapedh->c_pedido=$numero_pedidos;
            $sivapedh->c_numint=$sivapedm->row_id;
            $sivapedh->f_fecha=$request->f_fecha;
            $sivapedh->f_fechaped=$request->f_fecha;
            $sivapedh->c_tda=$tienda->c_codigo;
            $sivapedh->n_tcambio=$request->tipo_cambio;
            if($request->c_pagoen=="Soles"){
                $sivapedh->c_pagoen="NUEVOS SOLES";
            }else{
                $sivapedh->c_pagoen="DOLARES AMERICANOS";
            }

            $sivapedh->c_motivo=strtoupper($request->c_motivo);
            $sivapedh->c_tele=$request->c_tele;
            $sivapedh->c_empresa=$request->c_descrip;
            $sivapedh->c_empresa=$request->c_descrip;

            $sivapedh->c_codauxi=$det['c_codauxi'];
            $sivapedh->n_despa='0';
            $sivapedh->c_alm='';

            $sivapedh->c_codigo=$det['c_codigo'];
            $sivapedh->c_codigo1=$det['c_codigo1'];
            $sivapedh->c_codigo2=$det['c_codigo2'];
            $sivapedh->c_descrip=$det['c_descrip'];
            $sivapedh->c_descrip1=$det['c_descrip'];
            $sivapedh->n_packing=$det['n_packing'];
            if($det['docena']>0.00){
                $total_docena=$det['docena']*12;
                $sivapedh->n_dscto=$total_docena;
                $sivapedh->n_bulto=$total_docena;
                $suma_n_bulto=$suma_n_bulto+$total_docena;

                $sivapedh->n_todespa=$total_docena;
                $sivapedh->n_tocos=number_format($det["n_realcos"]*$det['docena'],2);
                $sivapedh->c_tipouni="D";
                $sivapedh->c_unipar='D';
            }
            if($det['caja']>0.00){
                if($sivapedh->c_unidad=="DOC"){
                    $total_paking=$det['n_packing']*12;
                }else{
                    $total_paking=$det['n_packing'];
                }
                $sivapedh->n_dscto=$total_paking*$det['caja'];
                $sivapedh->n_bulto=$total_paking*$det['caja'];
                $suma_n_bulto=$suma_n_bulto+($total_paking*$det['caja']);
                $sivapedh->n_todespa=$total_paking*$det['caja'];
                $sivapedh->n_tocos=$det["n_realcos"]*$det['caja'];
                $sivapedh->c_tipouni="C";
                $sivapedh->c_unipar='C';
            }

            if($det['unidad']>0.00){
                $sivapedh->n_dscto=$det['unidad'];
                $sivapedh->n_bulto=$det['unidad'];
                $suma_n_bulto=$suma_n_bulto+$det['unidad'];
                $sivapedh->n_todespa=$det['unidad'];
                $sivapedh->n_tocos=$det["n_realcos"]*$det['unidad'];
                $sivapedh->c_tipouni='U';
                $sivapedh->c_unipar='U';

            }
            $sivapedh->c_unidad=$det['c_unidad'];
            $sivapedh->n_caja=$det['caja'];
            $sivapedh->n_docena=$det['docena'];
            $sivapedh->n_cantidad=$det['unidad'];
            $sivapedh->n_preciocosto=$det["n_realcos"];

            $sivapedh->n_costo=$det["n_realcos"];
            $sivapedh->n_precio=$det['n_pventa'];
            $sivapedh->n_total=$det['total'];
            $sivapedh->n_cosori=$det['n_cosreal'];
            $suma_n_tocosori=number_format($suma_n_tocosori+$det['n_cosreal'],2);

            $sivapedh->c_generado="N";
            $sivapedh->c_estado="V";
            $sivapedh->c_movto="S";
            $sivapedh->c_anulado="N";
            $sivapedh->c_tipocli=0;
            $sivapedh->n_parcial=0.00;
            $sivapedh->c_undpar='';
            $sivapedh->n_pargen=0.00;
            $sivapedh->n_pardes=0;
            $sivapedh->c_empdes='';
            $sivapedh->c_nn='';
            $sivapedh->c_modi='';
            $sivapedh->c_refe='';
            $sivapedh->c_refe1='';
            $sivapedh->c_temp1='';
            $sivapedh->c_temp='';
            $sivapedh->c_prevta='';
            $sivapedh->c_usuario=$usuario->user_nomb;
            $sivapedh->c_tele='';
            $sivapedh->c_tipomon='';
            $sivapedh->c_codemp=$sivapedm->c_codemp;
            $sivapedh->save();
        }
        $sivapedm_update1=Sivapedm::findOrFail($sivapedm->row_id);
        $sivapedm_update1->n_bulto=$suma_n_bulto;
        $sivapedm_update1->n_tocosori=$suma_n_tocosori;
        $sivapedm_update1->save();
        DB::commit();
        $comprobante->numero=$correlativo->numero+1;
        $comprobante->save();
        return response()->json(['success'=>true,'msg'=>'Se Guardó la venta con Exito','id_venta'=>$sivapedm->row_id,'num_pedido'=>$numero_pedidos],201);


        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['success'=>false,'msg'=>$e],201);
        }

    }
    public function seach(Request $request){
       $sivacodm=Sivacodm::where(DB::raw("concat(TRIM(c_codigo),' ',TRIM(c_codigo2),' ',TRIM(c_descrip))"),'like',"%".$request->buscar."%")->get();
     return compact('sivacodm');
    }
    public function saldo_disponibles(Request $request){
//        $varios5=" C_CODIGO,C_CODEMP,SUM(N_DSCTO) AS N_DSCTO FROM sivafbph where C_CODAUXI = '".$request->codauxi."' AND C_CODIGO = '".$request->codigo."' and c_anulado = 'N' GROUP BY C_CODIGO,C_CODEMP";
    }
    public function seach_dni(Request $request){
        $client = new Client();
        $token="23e69d028b1b648589cfd7dcdd098adefde53715a680d4810a1370f3391524c1";
         if(count(Sivamed::where('c_ruc','=',$request->c_rucemp)->get())==0){
             if(strlen($request->c_rucemp)==11){
                $url = 'https://apiperu.dev/api/ruc/' .$request->c_rucemp;
        }else{
            $url = 'https://apiperu.dev/api/dni/' .$request->c_rucemp;
          }
          $response = $client->request('GET', $url, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json'
            ]
        ]);

        $json = json_decode($response->getBody()->getContents());
//      $json = $response->getBody()->getContents();
//      $json = json_decode($json);
    if ( $json->success ) {
        //Guardar el nuevo DNI/ RUC en la base de datos
               $cliente=new Sivamed();
               $json = $json->data;
               if(strlen($request->c_rucemp)==11){
                $cliente->c_ruc=$json->ruc;
                $cliente->c_descrip=$json->nombre_o_razon_social;
                $cliente->c_direcci=$json->direccion_completa;
                }else{
                    $cliente->c_ruc=$request->c_rucemp;
                    $cliente->c_descrip=$json->nombres." ".$json->apellido_paterno." ".$json->apellido_materno;
               }
               $cliente->save();
               $sivamed=Sivamed::where('c_ruc','=',$request->c_rucemp)->first();
               return compact('sivamed');
             }else{
                 //Mostrar el mensaje de error
                return ['success'=>false,'msg'=>$obj->message];
             }
//             return $datos;

        }else{
            $sivamed=Sivamed::where('c_ruc','=',$request->c_rucemp)->first();
            return compact('sivamed');

        }
    }
    public function tables(){
        $sivamed =Sivamed::where('c_clave','=','CLI')->paginate(10);
        $date = Carbon::now();
        $date = $date->format('Y-m-d');
        $tipocambio=Sivatpc::where('f_fecha','=',$date)->get();
        $sivacodm =Sivacodm::all();
        $tiendaactiva=Sivamed::where('row_id','=',session('c_codtienda'))->get();
        return compact('sivamed','sivacodm','tipocambio','tiendaactiva');
    }
    public function update(Request $request)    {
    }

    public function destroy($id) {
    }
}
