<?php

namespace App\Http\Controllers;

use App\sivacodm;
use Illuminate\Http\Request;

class SivacodmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function search(Request $request){

    }

    public function create()    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sivacodm  $sivacodm
     * @return \Illuminate\Http\Response
     */
    public function show(sivacodm $sivacodm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sivacodm  $sivacodm
     * @return \Illuminate\Http\Response
     */
    public function edit(sivacodm $sivacodm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sivacodm  $sivacodm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sivacodm $sivacodm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sivacodm  $sivacodm
     * @return \Illuminate\Http\Response
     */
    public function destroy(sivacodm $sivacodm)
    {
        //
    }
}
