<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sivanro extends Model
{
    protected $table='sivanro';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'fecha',
        'tienda',
        'numero',
        'nomtabla',
        'fsistema'
    ];
}
