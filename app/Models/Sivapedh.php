<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Sivapedh extends Model
{
    protected $table='sivapedh';
    protected $primaryKey = 'row_id';
    public $timestamps = false;
    protected $fillable = [
        'n_todespa',
        'n_despa',
        'c_tda',
        'c_alm',
        'c_codauxi',
        'c_codigo',
        'c_codigo1',
        'c_codigo2',
        'c_descrip',
        'c_descrip1',
        'n_packing',
        'c_unidad',
        'c_tipouni',
        'n_caja',
        'n_docena',
        'n_cantidad',
        'n_dscto',
        'n_costo',
        'n_precio',
        'n_total',
        'n_tocos',
        'n_cosori',
        'n_preciocosto',
        'c_tipomon',
        'c_docum',
        'c_pedido',
        'c_numint',
        'c_codemp',
        'c_empresa',
        'c_usuario',
        'f_fecha',
        'f_fechaped',
        'f_fechagra',
        'n_bulto',
        'c_movto',
        'c_anulado',
        'c_generado',
        'c_estado',
        'c_tipocli',
        'n_parcial',
        'c_undpar',
        'n_pargen',
        'n_pardes',
        'c_empdes',
        'c_nn',
        'c_modi',
        'c_refe',
        'c_refe1',
        'c_temp',
        'c_temp1',
        'n_tcambio',
        'c_motivo',
        'c_pagoen',
        'c_prevta',

    ];
}
