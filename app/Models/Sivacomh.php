<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sivacomh extends Model
{
    protected $table='sivacomh';
    protected $primaryKey = 'row_id';
    public $timestamps = false;
    protected $fillable = [
        'c_codfam',
        'c_familia',
        'c_codauxi',
        'c_codigo',
        'c_codigo1',
        'c_codigo2',
        'c_descrip',
        'c_descrip1',
        'c_unidad',
        'n_packing',
        'c_tipouni',
        'n_cantidad',
        'n_costo',
        'n_porvta',
        'n_precio',
        'n_preciound',
        'n_total',
        'n_cosreal'
    ];
}
