<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoCambio extends Model
{


    public $timestamps = false;
    protected $table='sivatpc';
    protected $fillable = [
        'f_fecha',
        'n_cambio',
        'n_com',
        'n_ven',
        'n_vta',
        'f_sistema',
        'c_usuario',
        'c_obs',
        'f_fechagra'
    ];

}
