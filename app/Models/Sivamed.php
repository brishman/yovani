<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Sivamed extends Model{
    protected $table='sivamed';
    protected $primaryKey = 'row_id';
    public $timestamps = false;
    protected $fillable = [
        'c_clave',
        'c_codigo',
        'c_subcod',
        'c_descrip',
        'c_direcci',
        'c_ruc',
        'c_telefo',
        'f_fecha',
        'f_fechagra',
        'c_visto',
        'c_tipocli',
        'c_inactiva',
        'c_inactivo',
        'c_fono2',
        'c_fono3',
        'c_observa',
        'c_email',
        'c_distrito',
        'n_lineacre',
        'n_totalcre',
        'n_totalcom',
        'n_credito',
        'n_pagos'
    ];
}
