<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Sivapedm extends Model
{
    protected $table='sivapedm';
    protected $primaryKey = 'row_id';
    public $timestamps = false;
    protected $fillable = [
        'c_tda',
        'c_codvta',
        'c_clientevta',
        'c_codemp',
        'c_empresa',
        'c_direcci',
        'c_rucemp',
        'c_codigo2',
        'c_correocli',
        'c_docum',
        'c_pedido',
        'c_numint',
        'f_fecha',
        'f_fechagra',
        'n_tcambio',
        'c_moneda',
        'c_motivo',
        'n_margen',
        'c_usuario',
        'n_total',
        'c_total',
        'n_bulto',
        'n_tocosori',
        'c_movto',
        'c_anulado',
        'n_credito',
        'n_lincre',
        'n_dscto',
        'c_generado',
        'c_estado',
        'c_tipocli',
        'c_telefono',
        'c_tele',
        'c_pagoen',
        'c_vendedor',
        'c_venpedido',
        'f_fechaeli',
        'c_dias',
        'c_tm'
    ];
}
