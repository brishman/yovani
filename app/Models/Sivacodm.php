<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Sivacodm extends Model{
protected $table='sivacodm';
protected $primaryKey = 'row_id';
public $timestamps = false;
protected $fillable = [
    'l_nofac',
    'l_facund',
    'l_facpro',
    'n_realpre',
    'n_realcos',
    'c_codauxi',
    'c_codigo',
    'c_codigo1',
    'c_codigo2',
    'c_descrip',
    'c_descrip1',
    'n_packing',
    'n_stopza',
    'f_fingreso',
    'f_sistema',
    'n_pventa',
    'c_usuario',

];
}
