<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    use Notifiable;
    public $timestamps = false;
    protected $table='tbuser';
    protected $primaryKey = 'id';
    protected $fillable = ['user_nomb','password','user_nive'];
    protected $hidden=[
        'password','remember_token'
    ];
}
