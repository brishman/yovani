<?php
Route::group(['middleware'=>['guest']], function () {
	Route::get('/', 'Auth\LoginController@showLoginForm');
	Route::post('/login','Auth\LoginController@login')->name('login');
});
Route::group(['middleware'=>['auth']], function () {
    Route::get('/sales', function () {
        return view('layouts.sales.index');
    });
    Route::get('/logout', 'LoginController@logout')->name('logout');
    Route::get('/sales', 'SivapedmController@index')->name('sales.dashboard');
    Route::get('/sales/seach', 'SivapedmController@seach');
    Route::get('/sales/buscar_costo', 'SivapedmController@buscar_costo');
    Route::get('/sales/tables', 'SivapedmController@tables');
    Route::get('/sales/seach_dni', 'SivapedmController@seach_dni');
    Route::post('/sales', 'SivapedmController@save');
    Route::get('/sales/filter_sivamed', 'SivamedController@search_sivamed');
    Route::post('/sales/register_sivamed', 'SivapedmController@register_sivamed');
});
Route::get('/sivamed/tienda', 'SivamedController@tienda');
