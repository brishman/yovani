<header class="header">
    <div class="logo-container">
        <a href="#" class="logo pt-2 pt-md-0">
                <i class="fa fa-circle fa-3x"></i>
        </a>
        <div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>
    <div class="header-right">
        <span class="separator"></span>
        <div id="userbox" class="userbox">

                <figure class="profile-picture">
                    <div class="border rounded-circle text-center" style="width: 25px;"><i class="fas fa-home"></i></div>
                </figure>
                <div class="profile-info">
                    <span class="name"></span>
                    <span class="role"> <b>{{ Session::get('c_tienda')}}</b></span>
                </div>

        </div>
        <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown">
                <figure class="profile-picture">
                    <div class="border rounded-circle text-center" style="width: 25px;"><i class="fas fa-user"></i></div>
                </figure>
                <div class="profile-info">
                    <span class="name"></span>
                    <span class="role">{{ Auth::user()->user_nomb }}</span>
                </div>
                <i class="fa custom-caret"></i>
            </a>
            <div class="dropdown-menu">
                <ul class="list-unstyled mb-2">
                    <li class="divider"></li>
                    <li>
                        {{--<a role="menuitem" href="#"><i class="fas fa-user"></i> Perfil</a>--}}
                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                          <i class="fas fa-power-off"></i>  Cerrar Session
                        </a>
                        <form id="logout-form" action="logout" method="GET" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>
