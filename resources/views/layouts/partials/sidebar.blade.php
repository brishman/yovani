<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">
    <div class="sidebar-header">
        <div class="sidebar-title">
            Menu
        </div>
        <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>
    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                    <li>
                        <a class="nav-link" href="http://demo.rmfact.info/dashboard">
                            <i class="fas fa-tachometer-alt" aria-hidden="true"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="
                        nav-parent
                        nav-active nav-expanded">
                        <a class="nav-link" href="#">
                            <i class="fas fa-receipt" aria-hidden="true"></i>
                            <span>Ventas</span>
                        </a>
                        <ul class="nav nav-children" style="">
                            <li class="">
                                <a class="nav-link" href="http://demo.rmfact.info/documents/create">
                                    Venta
                                </a>
                            </li>
                            <li class="">
                                <a class="nav-link" href="http://demo.rmfact.info/documents">
                                    Listados
                                </a>
                            </li>
                            <li class="">
                                <a class="nav-link" href="http://demo.rmfact.info/payments">
                                    Pagos Recibidos
                                </a>
                            </li>
                            <li class="">
                                <a class="nav-link" href="http://demo.rmfact.info/pos">
                                    Punto de Venta
                                </a>
                            </li>
                            <li class="">
                                <a class="nav-link" href="http://demo.rmfact.info/sale-notes">
                                    Nota de Venta
                                </a>
                            </li>
                            <li class="

                                ">
                                <a class="nav-link" href="http://demo.rmfact.info/summaries">
                                    <span>Resúmenes</span>
                                </a>
                            </li>
                            <li class="">
                                <a class="nav-link" href="http://demo.rmfact.info/quotations">
                                    Cotizaciones
                                </a>
                            </li>
                        </ul>
                    </li>

                    </li>
                </ul>
            </nav>
        </div>

    </div>
</aside>
<style>

</style>
<!-- end: sidebar -->

