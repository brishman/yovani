export const messagebox = {
    methods: {
        messagebox(url,msg_text,title_msg,msg_sucess,msg_error,text_button) {
            return new Promise((resolve) => {
                this.$confirm(msg_text,title_msg, {
                    confirmButtonText: text_button,
                    cancelButtonText: 'Cancelar',
                    type: 'warning'
                }).then(() => {
                    this.$http.delete(url)
                        .then(res => {
                            if(res.data.success) {
                                this.$message.success(msg_sucess)
                                resolve()
                            }
                        })
                        .catch(error => {
                            if (error.response.status === 500) {
                                this.$message.error(msg_error);
                            } else {
                                console.log(error.response.data.message)
                            }
                        })
                }).catch(error => {
                    console.log(error)
                });
            })
        },
    }
}
