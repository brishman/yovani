require('./bootstrap');
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import Axios from 'axios'
import lang from 'element-ui/lib/locale/lang/es'
import locale from 'element-ui/lib/locale'
locale.use(lang)
Vue.use(ElementUI, {size: 'small'})
Vue.prototype.$eventHub = new Vue()
Vue.prototype.$http = Axios
Vue.component('component-login', require('./components/login.vue').default);
Vue.component('example-component', require('./components/form.vue').default);

const app = new Vue({
    el: '#app',
});


